---
title: "Simple is better than complex"
date: 2019-10-21T18:54:51+02:00
draft: false
---

### The Zen of Python, by Tim Peters

```text
Simple is better than complex.
```

```text
Complex is better than complicated.
```

