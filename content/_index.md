---
draft: false
---

This project started as a compilation of js clean code tips from [clean-code-javascript github repo](https://github.com/ryanmcdermott/clean-code-javascript) with an idea of a [single page](/r/) which will rotate articles in random order with some timeout.

Later I decided to add more content from different areas and languages e.g. [Zen of Python](/python-zen/).

You can find my other projects here: [34x.me](https://34x.me)