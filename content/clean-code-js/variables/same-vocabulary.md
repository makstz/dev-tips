---
title: "Use the same vocabulary for the same type of variable"
date: 2019-10-20T20:53:57+02:00
draft: false
---

### Use the same vocabulary for the same type of variable

**Bad:**

```javascript
getUserInfo();
getClientData();
getCustomerRecord();
```

**Good:**

```javascript
getUser();
```