---
title: "Use meaningful and pronounceable variable names"
date: 2019-10-20T20:53:57+02:00
draft: false
---

### Use meaningful and pronounceable variable names

**Bad:**

```javascript
const yyyymmdstr = moment().format("YYYY/MM/DD");
```

**Good:**

```javascript
const currentDate = moment().format("YYYY/MM/DD");
```