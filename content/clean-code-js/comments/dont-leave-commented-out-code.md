---
title: "Don't leave commented out code in your codebase"
date: 2019-10-21T19:42:57+02:00
draft: false
---

### Don't leave commented out code in your codebase

Version control exists for a reason. Leave old code in your history.

**Bad:**

```javascript
doStuff();
// doOtherStuff();
// doSomeMoreStuff();
// doSoMuchStuff();
```

**Good:**

```javascript
doStuff();
```