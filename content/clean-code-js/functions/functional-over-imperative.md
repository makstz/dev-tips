---
title: "Favor functional programming over imperative programming"
date: 2019-10-21T20:53:57+02:00
draft: false
---

### Favor functional programming over imperative programming

**Bad:**

```javascript
const programmerOutput = [
  { name: "Uncle Bobby", linesOfCode: 500 },
  { name: "Suzie Q", linesOfCode: 1500 },
  { name: "Jimmy Gosling", linesOfCode: 150 },
  { name: "Gracie Hopper", linesOfCode: 1000 }
];

let totalOutput = 0;

for (let i = 0; i < programmerOutput.length; i++) {
  totalOutput += programmerOutput[i].linesOfCode;
}
```

**Good:**

```javascript
const programmerOutput = [
  { name: "Uncle Bobby", linesOfCode: 500 },
  { name: "Suzie Q", linesOfCode: 1500 },
  { name: "Jimmy Gosling", linesOfCode: 150 },
  { name: "Gracie Hopper", linesOfCode: 1000 }
];

const totalOutput = programmerOutput.reduce(
  (totalLines, output) => totalLines + output.linesOfCode,
  0
);
```

JavaScript isn't a functional language in the way that Haskell is, but it has
a functional flavor to it. Functional languages can be cleaner and easier to test.
Favor this style of programming when you can.