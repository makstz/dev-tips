---
title: Links
menu: ["main"]
---

## IT:

- [Design patterns and refactoring techiques](https://refactoring.guru)
- [Tech, analytical and near IT articles](https://habr.com)
- [Big O notation, time and space complexity explained](https://www.interviewcake.com/article/swift/big-o-notation-time-and-space-complexity)

## Other:

[Physical attraction podcast](https://physicalattraction.libsyn.com) - The show that tries to explain issues in physics, science, and technology - from the birth of stars to the end of the world.