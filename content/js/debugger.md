---
title: "Debugger"
date: 2019-10-31T09:09:42+01:00
draft: false 
---

# Try `debugger` for more complex cases.

If you use `console.log` for small debugger purposes try `debugger` in the same way! It will stop the app on this line and show a lot of useful info about the app state in the developer tools.

Just open developer tools and reload the page.


```javascript
const a = foo.a;
const b = foo.bar();

debugger;

// ....

```

