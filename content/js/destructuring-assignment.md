---
title: "Destructuring assignment"
date: 2019-10-21T14:53:57+02:00
draft: false
---

Be aware that:

```javascript
{ foo = 42 } = {};
// foo = 42
```

But:

```javascript
{ foo = 42 } = { foo: null };
// foo = null
```