function runTimer(params) {
	var config = Object.assign({
		count: 10,
		tick: function (params) { console.log('Iteration=' + params.current + ', left=' + params.left); },
		completed: function () { console.log('Ding dong'); }
	}, params);

	var intervalMs = 1000;
	var totalIterations = config.count;

	var currentIteration = 0;

	config.tick({
		current: currentIteration,
		left: totalIterations - currentIteration,
		total: totalIterations
	});

	setInterval(function() {
	  currentIteration++;
	  if (currentIteration >= totalIterations) {
	    config.completed();
	  }

	  config.tick({
	  	current: currentIteration,
	  	left: totalIterations - currentIteration,
	  	total: totalIterations
	  });
	}, intervalMs);
}

function showCountdown(params) {
	var el = document.getElementById('countdown');
	if (el) {
		el.innerText = params.left;
		// el.innerText = '.'.repeat(params.left);
	}
}